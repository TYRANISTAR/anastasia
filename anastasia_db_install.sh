#!/usr/bin/bash
#make sure you ran anastasia_tool_install.sh first
cur_dir=$(pwd)
cur_user=$USER
extra_dir=$(pwd)/ANASTASIA/anastasia_mods
soft_dir=$(pwd)/anastasia_software

echo "Installing NCBI-nr, NCBI-nt, Uniprot and Pfam databases for ANASTASIA tools... This process may take hours so allow overnight processing."
cd $soft_dir
mkdir ncbi_databases
mkdir Pfam_database
#download Pfam and format it for use with HMMER
echo "Downloading Pfam"
cd Pfam_database
wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz
gunzip Pfam-A.hmm.gz
hmmpress Pfam-A.hmm
#download nt and format it for use with BLAST
cd ../ncbi_databases
echo "Downloading NCBI-nT"
mkdir nt_database
cd nt_database
cp $cur_dir/ANASTASIA/update_blastdb.pl ./
perl update_blastdb.pl nt
tar -zxvf nt.01.tar.gz
makeblastdb -in nt -parse_seqids -dbtype nucl
#download nt and format it for use with BLAST
cd ..
echo "Downloading NCBI-nr"
mkdir nr_database
cd nr_database
cp $cur_dir/ANASTASIA/update_blastdb.pl ./
perl update_blastdb.pl nr
tar -zxvf nr.01.tar.gz
makeblastdb -in nr -parse_seqids -dbtype prot
#download uniprot and format it for use with BLAST
echo "Downloading Uniprot/SwissProt"
cd ..
mkdir uniprot_database
cd uniprot_database
wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz
gunzip uniprot_sprot.fasta.gz
makeblastdb -in uniprot_sprot -parse_seqids -dbtype prot

cd $cur_dir
sudo chmod -R 775 $soft_dir
sudo chown -R $cur_user:anastasia_group $soft_dir
echo "Databases installed in directory $soft_dir"
#-----------------------------
