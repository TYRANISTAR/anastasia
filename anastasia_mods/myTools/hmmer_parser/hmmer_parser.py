#!/usr/bin/python
#
#usage: python hmmer_parser.py hmmer.out dump.sql database_datafile

import sys, getopt, parser, csv, MySQLdb, random, string, subprocess, datetime
from Bio import SeqIO

assert sys.version_info[:2] >= ( 2, 4 )

#create a unique identifier (random variable for id) for each user
letandnum='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
unique_id= "".join(random.sample(letandnum, 15))
	
#check the date
day=str(datetime.date.today())
day=day.replace('-','_')
unique_id=unique_id+'_'+day

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()
	
def __main__():	
	#MySQL connection
	#dbs_db=sys.argv[3]
	#dbs_user=sys.argv[4]
	#dbs_passwd=sys.argv[5]	

	database_datafile = open(sys.argv[3], 'r')
	for i,line in enumerate(database_datafile):
		if i==0:
			dbs_db=line.replace('\n','')
		elif i==1:
			dbs_user=line.replace('\n','')
		elif i==2:
			dbs_passwd=line.replace('\n','')
		elif i>2:
			break
	database_datafile.close()

	db = MySQLdb.connect(host="127.0.0.1", user=dbs_user, port=3306, passwd=dbs_passwd, db=dbs_db)
	
	#cursor object that allows MySQL queries
	cur = db.cursor()
	
	#create table for hmmer results command
	#checking whether format is per domain or per sequence
	if len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==19:
		hmmer_tbl="create table %s_hmmer (target_name varchar(100), target_accession varchar(50), query_name varchar(100), query_accession varchar(50), evalue_full real, score_full real, bias_full real, evalue_best real, score_best real, bias_best real, exp real, reg integer(2), clu integer(2), ov integer(2), env integer(2), dom integer(2), rep integer(2), inc integer(2), description varchar(100))" % (unique_id,)
	elif len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==23:
		hmmer_tbl="create table %s_hmmer (target_name varchar(100), target_accession varchar(50), target_length integer, query_name varchar(100), query_accession varchar(50),query_length integer, evalue real, score real, bias real, domain_no integer, ndom integer, c_evalue real, i_evalue real, domain_score real, domain_bias real, from_hmm integer, to_hmm integer, from_ali integer, to_ali integer, from_env integer, to_env integer, acc real, description varchar(100))" % (unique_id,)
	else:
		stop_err("Your tabular file from hmmer is not of an appropriate format")
		
		
	#drop tables commands	
	drop_hmmer_tbl="drop table if exists %s_hmmer" % (unique_id,)
	drop_hmmer="drop table %s_hmmer" % (unique_id,)
	
	#MySQL create table
	cur.execute(drop_hmmer_tbl)		#if exists
	cur.execute(hmmer_tbl)			
	
	#hmmer		
	if len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==19:
		for row in csv.reader(open(sys.argv[1], 'r'), delimiter='	'):
			cur.execute('insert into %s_hmmer (target_name, target_accession, query_name, query_accession, evalue_full, score_full, bias_full, evalue_best, score_best, bias_best, exp, reg, clu, ov, env, dom, rep, inc, description) values %s' % (unique_id, str(tuple(row))))
			db.commit()
	elif len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==23:		
		for row in csv.reader(open(sys.argv[1], 'r'), delimiter='	'):
			cur.execute('insert into %s_hmmer (target_name, target_accession, target_length, query_name, query_accession, query_length, evalue, score, bias, domain_no, ndom, c_evalue, i_evalue, domain_score, domain_bias, from_hmm, to_hmm, from_ali, to_ali, from_env, to_env, acc, description) values %s' % (unique_id, str(tuple(row))))
			db.commit()
	cur.execute("delete from %s_hmmer where target_accession='accession'" %(unique_id,))

	#mysqldump
	subprocess.call('mysqldump -u %s -p%s %s %s_hmmer > %s' % (dbs_user, dbs_passwd, dbs_db, unique_id, sys.argv[2]), shell=True)
	
	#drop mysql tables for house cleaning	
	cur.execute(drop_hmmer)
	
if __name__ == "__main__": __main__()
