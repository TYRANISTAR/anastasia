#!/usr/bin/python
#
# usage:python gene_coverage.py gff_file sorted_bam_file output_hist output_coverage bed_file
#
#gff_file: PROKKA gff output file
#sorted_bam_file: sorted BAM file from mapping the reads back to the contigs
#output_hist: histogram of results
#output_coverage: list of coverage for genes found via PROKKA pipeline
#bed_file: gene regions from PROKKA output in suitable format

import subprocess, sys, random, string, os
from shutil import copyfile

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()

#to do: add more input variables for bowtie-build
def __main__():	
	unique_id= "".join(random.sample(string.letters, 12))

	gff_file = sys.argv[1]
	sorted_bam_file = sys.argv[2]
	output_hist = sys.argv[3]
	output_coverage = sys.argv[4]
	bed_file = sys.argv[5]
	
	prokka2gff_path='/home/ladoukef/programs_various/' #directory of the customized script prokkagff2bed.sh
	get_coverage_path='/home/ladoukef/programs_various/'#directory of the customized script get_coverage_for_genes.py
	bedtools_path='/home/ladoukef/programs_various/bedtools2/bin/' #directory of bedtools
	
	#use customized script to search for the gene regions in PROKKA .gff output file
	subprocess.call('sh %sprokkagff2bed.sh %s > %s' %(prokka2gff_path, gff_file, bed_file), shell=True)
	#extract coverage using bedtools
	subprocess.call("%sbedtools coverage -hist -a %s -b %s > %s" %(bedtools_path, bed_file, sorted_bam_file, output_hist), shell=True)
	#calculate coverage per gene
	subprocess.call('python %sget_coverage_for_genes.py -i %s > %s' %(get_coverage_path, output_hist, output_coverage), shell=True)

	
	
if __name__ == "__main__": __main__()



