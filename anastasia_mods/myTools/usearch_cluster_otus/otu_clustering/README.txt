Galaxy wrappers for USEARCH - UPARSE (OTU Clustering) 
=====================================================

USEARCH requires a licence. Therefore an automated installation is not
possible at the moment.

Requirements
=====================================================
Get your licenced USEARCH version 7, but not later then 7.0.1002 from here:
http://www.drive5.com/usearch/download.html


Manual Installation Steps
=====================================================
USEARCH is distributed as one file, known as the binary file or executable
file. It is completely self-contained: it does not require configuration
files, environment variables, third-party libraries or other external
dependencies. There is no setup script or installer because they're not
needed. To install it, all you do is download or copy the binary to a
directory that is accessible from the computer where you want to run the code.

Step1:
Rename the binary file to usearch.

Step2:
Move the binary file (usearch) to /usr/local/bin
Ensure /usr/local/bin is in your path. If needed add /usr/local/bin/ to your
path.

Step3:
Ensure that you have read and execute permissions for the binary file.
If needed, use the chmod command to set the execute bit, e.g.:
chmod +x /usr/local/bin/usearch


Further installation information and help can be found at:
http://drive5.com/usearch/manual/install.html


Disclaimer
=====================================================
This source code is provided by QFAB Bioinformatics "as is", in the hope that it will be
useful, and any express or implied warranties, including, but not limited to,
the implied warranties of merchantability and fitness for a particular purpose
are disclaimed. 
IN NO EVENT SHALL QFAB BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOURCE
CODE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License
=====================================================
This work by QFAB Bioinformatics (as part of the GVL project http://genome.edu.au) 
is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License.
