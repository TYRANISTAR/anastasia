#!/usr/bin/python
#
# usage:python count_mapped.py bam_file mapped output_file
#
#bam_file: the output file from bowtie2 in bam format
#mapped: 'mapped' if you want to count the mapped reads, 'unmapped' if you want to count the unmapped reads
#output_file: the file that gets the number of reads

import subprocess, sys

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()

#to do: add more input variables for bowtie-build
def __main__():	
	bam_file = sys.argv[1]
	mapped = sys.argv[2]
	output_file = sys.argv[3]
	output_f = open(output_file, 'a')

	samtools_path='' #directory of samtools executables (leave blank if they're included in your $PATH)
	
	#count reads
	if mapped=='mapped':
		output_f.write("Number of mapped reads"+"\n")
		subprocess.call('%ssamtools view -c -F 4 %s >> %s' %(samtools_path,bam_file,output_file), shell=True)	
	if mapped=='unmapped':
		output_f.write("Number of unmapped reads"+"\n")
		subprocess.call('%ssamtools view -c -f 4 %s >> %s' %(samtools_path,bam_file,output_file), shell=True)
	

	
if __name__ == "__main__": __main__()


