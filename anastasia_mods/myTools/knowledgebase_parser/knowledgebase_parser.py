#!/usr/bin/python
#
# usage: python know_parser.py dump.sql title database_datafile output.txt
#
# A script for creating a new database viewable in a web2py knowledgebase via SQLFORM. The script calls 
# a script from web2py to write into the db.py (or whatever file the models are in) in order for the database
# to be included. "database_datafile" is a file containing the credentials of the database - first row of the 
# file includes the database, second row is the username and third row is the password.
#

import os, sys, parser, MySQLdb, random, string, datetime, re
from Bio import SeqIO

def stop_err(msg):
	sys.stderr.write("{0}\n".format(msg))
	sys.exit
	
def __main__():
	#create a unique identifier (random variable for id) for each user
	letandnum='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
	start_id= "knowledgebase_"
	unique_id=start_id+ "".join(random.sample(letandnum, 20))
	
	#check the date
	day=str(datetime.date.today())
	day=day.replace('-','_')
		
	#mysql dump
	mysql_dump=sys.argv[1]
	title=sys.argv[2]
	
	#MySQL connect
	#dbs_db='anastasia_knowledgebase'
	#dbs_user=sys.argv[3]
	#dbs_passwd=sys.argv[4]
	
	database_datafile = open(sys.argv[3], 'r')
	for i,line in enumerate(database_datafile):
		if i==0:
			dbs_db=line.replace('\n','')
		elif i==1:
			dbs_user=line.replace('\n','')
		elif i==2:
			dbs_passwd=line.replace('\n','')
		elif i>2:
			break
	database_datafile.close()		

	db=MySQLdb.connect(host="127.0.0.1", user=dbs_user, port=3306, passwd=dbs_passwd)

	#set output file
	output_file=sys.argv[4]	

	#cursor object that allows MySQL queries
	cur=db.cursor()
		
	#Path of the Web2py extract_mysql_models.py script directory
	mysqlscript_path='/home/web2py_usr/web2py/scripts/'
	
	#Path of the Web2py application
	web2py_app='/home/web2py_usr/web2py/applications/anastasia_knowledgebase/'
	
	#Name of the modified models/db.py script and the database
	web_db_model='akn_db.py'
	web_db_name='akn_db'

	#ANASTASIA knowledgebase link to tables
	know_link="http://motherbox.chemeng.ntua.gr/anastasia_knowledgebase/default/manage_makis?table_name="
	
	#Change the table names in the mysql dump to a random string followed by the date	
		
	#letandnum='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
	outfile = open(output_file,'a')
	outfile.write("<!DOCTYPE html>\n<html>\n<body>")
	with open(mysql_dump, 'r') as rrf:	
		count=0
		for line in rrf:
			if '-- Table structure for table' in line:
				count=count+1
		outfile.write('<p><strong>Your results consist of {0} dataset(s)</strong></p> \
		<p>----------------------------------------------------------------------------------------------------------------------------</p>\
		<p>----------------------------------------------------------------------------------------------------------------------------</p>'.format(count))
		count=0 #reset count
	with open(mysql_dump, 'r') as rf:		
		for line in rf:			
			if line.startswith('CREATE DATABASE  IF NOT EXISTS'):
				os.system('sed -i "s/{0}/{1}/g" {2}'.format('CREATE DATABASE','--CREATE DATABASE  IF NOT EXISTS', mysql_dump))
			elif line.startswith('USE '):
				os.system('sed -i "s/{0}/{1}/g" {2}'.format('USE ','--USE ', mysql_dump))
			elif '-- Table structure for table' in line:
				count=count+1
				unique_id=start_id+ "".join(random.sample(letandnum, 20))
				tbl_nm=unique_id+day+'_table'
				regexp = re.compile('Table structure for table `(.*?)`')
				m = regexp.search(line)
				id2w=m.group(1) 
				os.system('sed -i "s/{0}/{1}/g" {2}'.format(id2w,tbl_nm, mysql_dump))
				outfile.write('<p>    Your <a href="{0}">dataset no {2} </a> has been imported and can be viewed in \
					<strong>ANASTASIA knowledgebase</strong> by clicking on the following link:\
					</p><p><a href="http://motherbox.chemeng.ntua.gr/anastasia_knowledgebase/">http://motherbox.chemeng.ntua.gr/anastasia_knowledgebase/</a></p><p>    and entering\
					the following Job ID:</p><p>    {1}</p><p>    In order to be able to access \
					the database you need to have registered and have sufficient rights.</p>   \
					<p>----------------------------------------------------------------------------------------------------------------------------</p>	\
	'.format(know_link+tbl_nm,tbl_nm,count))
	outfile.write("\n</html>\n</body>")
	outfile.close()
	rf.close()
	
	#Import mysql dump file in a temporary mysql database
	cur.execute("create database if not exists {0};".format(dbs_db))
	#os.system('mysql -u {0} -p{1} {2}<{3}'.format(dbs_user, dbs_passwd, dbs_db, mysql_dump))
	cur.execute("create database if not exists {0};".format(unique_id+day+'tmp_database'))
	os.system('mysql -u {0} -p{1} {2}<{3}'.format(dbs_user, dbs_passwd, unique_id+day+'tmp_database', mysql_dump))
		

	#Add id column in every table of the temporary mysql database
	cur.execute("select concat('alter table ',table_name,' add column id int auto_increment not null primary key first;')\
	from information_schema.tables where table_schema = '{0}' and table_type = 'base table' into outfile '{1}';"\
	.format(unique_id+day+'tmp_database','/home/anastasia_dev/tmp/'+unique_id+day+'tmp_sql.sql'))

	tmpidsql='/home/anastasia_dev/tmp/'+unique_id+day+'tmp_sql.sql'
	
	try:
		os.system('mysql -u {0} -p{1} {2}<{3}'.format(dbs_user, dbs_passwd, unique_id+day+'tmp_database', tmpidsql))
	except:
		pass
		
	#Call the extract_mysql_models script (included in web2py) in order to append the model for the new existing web2py table and 
	#move the tables to user devined database
	os.system("python {0}extract_mysql_models.py {1}:{2}@{3} > /tmp/{4}.py"\
	.format(mysqlscript_path, dbs_user, dbs_passwd, unique_id+day+'tmp_database', unique_id+day))
	
	cur.execute("select concat('alter table {0}.',table_name,' rename {1}.',table_name,';')\
	from information_schema.tables where table_schema = '{0}' and table_type = 'base table' into outfile '{2}';"\
	.format(unique_id+day+'tmp_database',dbs_db,'/home/anastasia_dev/tmp/'+unique_id+day+'tmp_sql_mv.sql'))	
	tmpmvsql='/home/anastasia_dev/tmp/'+unique_id+day+'tmp_sql_mv.sql'
	os.system('mysql -u {0} -p{1} {2}<{3}'.format(dbs_user, dbs_passwd, unique_id+day+'tmp_database', tmpmvsql))
	
	cur.execute("drop database {0}".format(unique_id+day+'tmp_database')) #after having the new model file drop the temporary database
		
	#Make correction to the temporary file and change the name of the declared database
	os.system('sed -i "/legacy_db = DAL(/c\#--------" /tmp/{0}.py'.format(unique_id+day))
	os.system('sed -i "1i\n#--------" /tmp/{0}.py'.format(unique_id+day))
	os.system('sed -i "s/legacy_db/{0}/g" /tmp/{1}.py'.format(web_db_name, unique_id+day))
	
	#Concatenate the model file created to the models.py of web2py application and then remove the temporary file
	os.system('cat /tmp/{0}.py >> {1}models/{2}'.format(unique_id+day, web2py_app, web_db_model))
	os.system('rm /tmp/{0}.py'.format(unique_id+day))
	os.system('rm /home/anastasia_dev/tmp/{0}'.format(unique_id+day+'tmp_sql_mv.sql'))
	os.system('rm /home/anastasia_dev/tmp/{0}'.format(unique_id+day+'tmp_sql.sql'))
	

	print 'Your dataset with title {0} is ready'.format(title)
	#print 'Your model file is ready'
	
if __name__ == "__main__": __main__()	
	
