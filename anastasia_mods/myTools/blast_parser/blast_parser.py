#!/usr/bin/python
#
#usage: python blastp_parser.py blastp.tabular orfs.fasta dump.sql database_datafile orphans.fasta blastp_orf.fasta

import sys, getopt, parser, csv, MySQLdb, random, string, subprocess, datetime
from Bio import SeqIO
csv.field_size_limit(sys.maxsize)
assert sys.version_info[:2] >= ( 2, 4 )

#create a unique identifier (random variable for id) for each user
letandnum='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
unique_id= "".join(random.sample(letandnum, 15))
	
#check the date
day=str(datetime.date.today())
day=day.replace('-','_')
unique_id=unique_id+'_'+day

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()
	
def __main__():	
	#MySQL connection
	#dbs_db=sys.argv[4]
	#dbs_user=sys.argv[5]
	#dbs_passwd=sys.argv[6]	
	
	database_datafile = open(sys.argv[4], 'r')
	for i,line in enumerate(database_datafile):
		if i==0:
			dbs_db=line.replace('\n','')
		elif i==1:
			dbs_user=line.replace('\n','')
		elif i==2:
			dbs_passwd=line.replace('\n','')
		elif i>2:
			break
	database_datafile.close()	

	db = MySQLdb.connect(host="127.0.0.1", user=dbs_user, port=3306, passwd=dbs_passwd, db=dbs_db)
	
	#cursor object that allows MySQL queries
	cur = db.cursor()
	
	#create table for blastp results command
	#checking tabular format
	if len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==25:
		blastp_tbl="create table %s_blastp (query_id varchar(100), subject_id varchar(100), match_percentage float, align_length integer, mismatch_no integer, gap_openings integer, query_start integer, query_end integer, alignment_start integer, alignment_end integer, evalue real, bitscore real, seqid varchar(200), raw_score integer, ident_matches integer, posit_matches integer, gaps integer, positive_percent float, query_frame integer(1), subject_frame integer(1), query_aligned MEDIUMTEXT, subject_aligned MEDIUMTEXT, query_length integer, subject_length integer, description MEDIUMTEXT)" % (unique_id,)
	elif len(csv.reader(open(sys.argv[1], 'r'), delimiter='	').next())==12:
		blastp_tbl="create table %s_blastp (query_id varchar(100), subject_id varchar(100), match_percentage float, align_length integer, mismatch_no integer, gap_openings integer, query_start integer, query_end integer, alignment_start integer, alignment_end integer, evalue real, bitscore real)" % (unique_id,)
	else:
		stop_err("Your tabular file from blastp is not of an appropriate format")	
	
	#MySQL create table
	drop_blastp_tbl="drop table if exists %s_blastp" % (unique_id,)
	cur.execute(drop_blastp_tbl)	#if exists	
	drop_orf_tbl="drop table if exists %s_orf_tbl" % (unique_id,)	
	cur.execute(drop_orf_tbl)		#if exists
	drop_orphans_tbl="drop table if exists %s_orphans" % (unique_id,)	
	cur.execute(drop_orphans_tbl)	#if exists
	drop_orf_blast_hits_tbl="drop table if exists %s_orf_blast_hits" % (unique_id,)
	cur.execute(drop_orf_blast_hits_tbl)	#if exists
	cur.execute(blastp_tbl)
	#table for orf results command
	orf_tbl="create table %s_orf_tbl (orf_id varchar(100), sequence MEDIUMTEXT)" % (unique_id,)
	cur.execute(orf_tbl)	

	#transform FASTA to tab delimited
	f=open("/home/anastasia_dev/tmp/%s_fasta_tab_temp.txt" % (unique_id,),"a")
	for seq_record in SeqIO.parse(sys.argv[2], 'fasta'):
    		f.write('{}\t{}'.format(seq_record.description, seq_record.seq))
		f.write('\n')
	f.close()

	#orfs
	cur.execute("load data infile '/home/anastasia_dev/tmp/%s_fasta_tab_temp.txt' into table %s_orf_tbl" % (unique_id,unique_id))

	#blastp
	cur.execute("load data infile '%s' into table %s_blastp" % (sys.argv[1], unique_id))
			
	#mysql creating orphans and hits
	unique_blast="create table %s_unique_blastp_queries (select distinct query_id from %s_blastp)" % (unique_id, unique_id)
	cur.execute(unique_blast)
	
	orf_blast_hits="create table %s_orf_blast_hits select * from %s_orf_tbl c2t where exists (select * from %s_unique_blastp_queries c where c.query_id = c2t.orf_id )" % (unique_id, unique_id, unique_id)
	cur.execute(orf_blast_hits)

	orphans="create table %s_orphans select * from %s_orf_tbl c2t where not exists (select * from %s_unique_blastp_queries c where c.query_id = c2t.orf_id )" % (unique_id, unique_id, unique_id)
	cur.execute(orphans)
	
	#mysql to fasta for orphans and hits
	#first export to txt file
	orphan_dump="select orf_id, sequence from %s_orphans into outfile '/home/anastasia_dev/tmp/%s_orphans.txt'" % (unique_id, unique_id)
	cur.execute(orphan_dump)
	blastorfs_dump="select orf_id, sequence from %s_orf_blast_hits into outfile '/home/anastasia_dev/tmp/%s_blastorfs.txt'" % (unique_id, unique_id)
	cur.execute(blastorfs_dump)
	
	#then convert the txt file to fasta file
	SeqIO.convert('/home/anastasia_dev/tmp/%s_orphans.txt' % (unique_id,),'tab','%s' % (sys.argv[5],),'fasta') 
	SeqIO.convert('/home/anastasia_dev/tmp/%s_blastorfs.txt' % (unique_id,),'tab','%s' % (sys.argv[6],),'fasta') 
	
	#mysqldump
	subprocess.call('mysqldump -u %s -p%s %s %s_blastp %s_orf_tbl %s_orphans %s_orf_blast_hits > %s' % (dbs_user, dbs_passwd, dbs_db, unique_id, unique_id, unique_id, unique_id, sys.argv[3]), shell=True)
	
	#drop mysql tables for house cleaning	
	drop_blastp="drop table %s_blastp" % (unique_id,)
	cur.execute(drop_blastp)
	drop_orf="drop table %s_orf_tbl" % (unique_id,)
	cur.execute(drop_orf)
	drop_orphans="drop table %s_orphans" % (unique_id,)	
	cur.execute(drop_orphans)
	drop_orf_blast_hits="drop table %s_orf_blast_hits" % (unique_id,)
	cur.execute(drop_orf_blast_hits)
	
	#remove unecessary data from the temporary directory
	subprocess.call('rm /home/anastasia_dev/tmp/%s_orphans.txt' %(unique_id,), shell=True)
	subprocess.call('rm /home/anastasia_dev/tmp/%s_blastorfs.txt' %(unique_id,), shell=True)
	subprocess.call('rm /home/anastasia_dev/tmp/%s_fasta_tab_temp.txt' %(unique_id,), shell=True)
	
		
if __name__ == "__main__": __main__()

