#!/usr/bin/python
#
# usage: fasta_names.py orfile.fasta new_orfile.fasta
#
#takes a fasta file with long sequence names and replaces them with shorter_ones

import sys, getopt, parser, csv, random, string, subprocess
from Bio import SeqIO


assert sys.version_info[:2] >= ( 2, 4 )

unique_id= "".join(random.sample(string.letters, 12))


def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()
	
def __main__():	
	
	#orfs
	count = 0
	for seq_record in SeqIO.parse(sys.argv[1], 'fasta'):
		count = count + 1
		seq_record.id = "NEW_NODE_" + str(count)
		seq_record.description = ""
		with open(sys.argv[2], "a") as myfile:
			SeqIO.write(seq_record,myfile, "fasta")
    
		
if __name__ == "__main__": __main__()



