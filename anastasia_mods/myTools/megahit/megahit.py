#!/usr/bin/python
#
# usage:python megahit.py contigs_file result_directory.tar.gz min_contig_lgth cpus presets kind_of_reads read_file(s)
#
#
#
import subprocess, sys, random, string
from shutil import copyfile

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()

#to do: add more input variables for bowtie-build
def __main__():	
	unique_id= "".join(random.sample(string.letters, 12))

	contig_file = sys.argv[1]
	result_directory_tar = sys.argv[2]
	result_directory = '/tmp/%s_megahit/' % (unique_id)	
	min_contig_lgth = sys.argv[3]
	cpus = sys.argv[4]
	presets = sys.argv[5]
	kind_of_reads = sys.argv[6]
	
	if kind_of_reads=='paired':
		read1=sys.argv[7]
		read2=sys.argv[8]
	else:
		read=sys.argv[7]
	
	megahit_path='/home/ladoukef/programs_various/megahit-1.0.3/' #directory of megahit executables (leave blank if they're included in your $PATH)
	
	#call megahit
	if kind_of_reads=='paired': #paired end reads in separate files
		print 'paired'
		subprocess.call('%smegahit -1 %s -2 %s --presets %s -o %s -m 0.2 -t %s --min-contig-len %s' %(megahit_path, read1, read2, presets, result_directory, cpus, min_contig_lgth), shell=True)
	
	elif kind_of_reads=='inter': #paired end reads in interleaved file
		print 'inter'
		subprocess.call('%smegahit --12 %s --presets %s -o %s -m 0.2 -t %s --min-contig-len %s' %(megahit_path, read, presets, result_directory, cpus, min_contig_lgth), shell=True)
		
	elif kind_of_reads=='single': #single end reads
		print 'single'
		subprocess.call('%smegahit -r %s --presets %s -o %s -m 0.2 -t %s --min-contig-len %s' %(megahit_path, read, presets, result_directory, cpus, min_contig_lgth), shell=True)	
	else:
		print 'error'
	
	#move contig file
	subprocess.call("mv %sfinal.contigs.fa %s" %(result_directory,contig_file), shell=True)
	
	#tar all the rest result files
	subprocess.call("tar czf %s %s" %(result_directory_tar,result_directory), shell=True)
	subprocess.call("rm -rf %s" %(result_directory), shell=True)
	


		
	
if __name__ == "__main__": __main__()


