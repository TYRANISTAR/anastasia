#!/usr/bin/python
#
# usage:python prokka.py contigs.fasta result_directory.tar.gz annotation_mode ec_numbers_file prokka_gff prokka_faa prokka_ffn rrna trna
#
#
#
import subprocess, sys, random, string
from shutil import copyfile

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()

#to do: add more input variables for bowtie-build
def __main__():	
	unique_id= "".join(random.sample(string.letters, 12))

	contig_file = sys.argv[1]
	result_directory_tar = sys.argv[2]
	result_directory = '/tmp/%s_prokka/' % (unique_id)	
	annotation_mode = sys.argv[3]
	ec_numbers_file = sys.argv[4]
	prokka_gff = sys.argv[5]
	prokka_faa = sys.argv[6]
	prokka_ffn = sys.argv[7]
	rrna = sys.argv[8]
	trna = sys.argv[9]
	prokka_path='/home/ladoukef/programs_various/prokka-1.11/bin/' #directory of prokka executables (leave blank if they're included in your $PATH)
	
	#prokka pipeline
	subprocess.call('%sprokka %s --outdir %s %s %s --kingdom %s --metagenome --cpus 1' %(prokka_path, contig_file, result_directory, rrna, trna, annotation_mode), shell=True)
	#get list of ec numbers from annotated sequences
	subprocess.call("grep 'eC_number=' %s*.gff | cut -f9 | cut -f1,2 -d ';'| sed 's/ID=//g'| sed 's/;eC_number=/\t/g' > %s" %(result_directory,ec_numbers_file), shell=True)
	#rename .gff file and PROKKA*.faa file
	subprocess.call("mv %sPROKKA*.gff %s" %(result_directory,prokka_gff), shell=True)
	subprocess.call("mv %sPROKKA*.faa %s" %(result_directory,prokka_faa), shell=True)
	subprocess.call("mv %sPROKKA*.ffn %s" %(result_directory,prokka_ffn), shell=True)
	subprocess.call("tar czf %s %s" %(result_directory_tar,result_directory), shell=True)
	subprocess.call("rm -r %s" %(result_directory), shell=True)
	
	



		
	
if __name__ == "__main__": __main__()


