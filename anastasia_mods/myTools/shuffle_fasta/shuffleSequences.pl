#!/usr/bin/perl

if (!@ARGV) {
	print "Usage: $0 forward_reads reverse_reads filetype outfile.fa\n";
	print "\tforward_reads / reverse_reads : paired reads to be merged\n";
	print "\tfiletype : 'fasta' or 'fastq'\n";
	print "\toutfile.fa : outfile to be created\n";
	system.exit(0);	
}

$filenameA = $ARGV[0];
$filenameB = $ARGV[1];
$filetype = $ARGV[2];
$filenameOut = $ARGV[3];

die "Could not open $filenameA" unless (-e $filenameA);
die "Could not open $filenameB" unless (-e $filenameB);

open FILEA, "< $filenameA";
open FILEB, "< $filenameB";

open OUTFILE, "> $filenameOut";


if ($filetype eq "fasta"){

my ($lineA, $lineB);

$lineA = <FILEA>;
$lineB = <FILEB>;

while(defined $lineA) {
	print OUTFILE $lineA;
	$lineA = <FILEA>;
	while (defined $lineA && $lineA !~ m/>/) { 
		print OUTFILE $lineA;
		$lineA = <FILEA>;
	}

	print OUTFILE $lineB;
	$lineB = <FILEB>;
	while (defined $lineB && $lineB !~ m/>/) { 
		print OUTFILE $lineB;
		$lineB = <FILEB>;
	}
}
}elsif ($filetype eq "fastq"){
	my ($lineA, $lineB);

$lineA = <FILEA>;
$lineB = <FILEB>;

while(defined $lineA) {
	print OUTFILE $lineA;
	$lineA = <FILEA>;
	while (defined $lineA && $lineA !~ m/@/) { 
		print OUTFILE $lineA;
		$lineA = <FILEA>;
	}

	print OUTFILE $lineB;
	$lineB = <FILEB>;
	while (defined $lineB && $lineB !~ m/@/) { 
		print OUTFILE $lineB;
		$lineB = <FILEB>;
	}
}
}else{ die "please insert the correct filetype"}
