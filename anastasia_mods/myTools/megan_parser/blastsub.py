#!/usr/bin/python
import sys
import subprocess
import random
import string
import os

unique_id= "".join(random.sample(string.letters, 12))

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)


query = sys.argv[1] # FASTA file of reads
blastfile = sys.argv[2] #Output file from BLAST in tab delimited format 
meganfile = sys.argv[3] #MEGAN .rma file. Output data
useseed = sys.argv[4] #MEGAN parameter - Enable SEED analysis
usecog = sys.argv[5] #MEGAN parameter - Enable COG analysis
usekegg = sys.argv[6] #MEGAN parameter - Enable KEGG analysis
maxmatches = sys.argv[7] #MEGAN parameter - The Max number of matches per read item specifies how many matches per read to save in the RMA file
taxgifile = '/home/ladoukef/programs_various/MEGAN/gi_taxid-March2015X.bin'
giseedfile = '/home/ladoukef/programs_various/MEGAN/gi2seed.map'
gikeggfile = '/home/ladoukef/programs_various/MEGAN/gi2kegg-Feb2015X.bin'

textfile=open('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id),'w')
textfile.write("load taxGIFile='/home/ladoukef/programs_various/MEGAN/gi_taxid-March2015X.bin' \nload keggRefSeqFile='ref2kegg.map'\nload seedRefSeqFile='ref2seed.map'\nload cogRefSeqFile='ref2cog.map'\nimport blastFile=" + (blastfile) + " fastaFile=" + (query) + " meganFile=" + (meganfile) + " maxMatches="+ (maxmatches)+ " minScore=5.0 maxExpected=0.01 topPercent=10.0 minSupport=1 minComplexity=0.0 useMinimalCoverageHeuristic=false"+" useSeed="+(useseed)+" useCOG="+(usecog)+" useKegg="+(usekegg)+" paired=false useIdentityFilter=false textStoragePolicy=Embed mapping='Taxonomy:BUILT_IN=true,Taxonomy:GI_MAP=true,SEED:REFSEQ_MAP=true,KEGG:REFSEQ_MAP=true,COG:REFSEQ_MAP=true';\nquit;\n")
textfile.close() #create a command file of MEGAN commands
subprocess.call ('xvfb-run --auto-servernum MEGAN -g -E -L /home/ladoukef/programs_various/MEGAN/MEGAN5-academic-license.txt < /home/anastasia_dev/tmp/'+unique_id+'_commandsall.txt ' , shell=True ) #call MEGAN and read commands from the file created
#os.remove ('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id)) # remove the file of MEGAN commands from tmp








 



     
