#!/usr/bin/python
import sys
import subprocess
import string
import os
import tarfile
import random
import shutil

unique_id= "".join(random.sample(string.letters, 12))

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)


meganfile = sys.argv[1] # tool input
tardata = sys.argv[2] # tar.gz output
chartdata1 = sys.argv[3] # chart image SEED output 
chartdata2 = sys.argv[4] # chart image COG output
chartdata3 = sys.argv[5] # chart image KEGG output
minscore =sys.argv[6] # MEGAN parameter
maxexpected = sys.argv[7] # MEGAN parameter
toppercent = sys.argv[8] # MEGAN parameter
minsupport = sys.argv[9] # MEGAN parameter
mincomplexity = sys.argv[10] # MEGAN parameter
use_minimal_coverage_heuristic = sys.argv[11] # MEGAN parameter
use_identity_filter = sys.argv[12] # MEGAN parameter
minsupportpercent = sys.argv[13] # MEGAN parameter
lcapercent = sys.argv[14] # MEGAN parameter
paired_reads = sys.argv[15] # MEGAN parameter


a=os.path.realpath(meganfile)
b=a+'.rma'
subprocess.call('cp %s %s' % (a,b) , shell=True)
meganfile=b # add the extension .rma to the input file

newpath = ('/home/anastasia_dev/tmp/%s_OutputsSEED' % (unique_id) )
os.makedirs(newpath) # create a directory for SEED outputs

newpath = ('/home/anastasia_dev/tmp/%s_OutputsCOG' % (unique_id) )
os.makedirs(newpath) # create a directory for COG outputs

newpath = ('/home/anastasia_dev/tmp/%s_OutputsKEGG' % (unique_id) )
os.makedirs(newpath) # create a directory for KEGG outputs

textfile=open('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id),'w')
textfile.write("open file=" + (meganfile) + ";\nrecompute minSupportPercent="+(minsupportpercent)+" minSupport="+(minsupport)+" minScore="+(minscore)+" maxExpected="+(maxexpected)+" topPercent="+(toppercent)+" lcaPercent="+(lcapercent)+" minComplexity="+(mincomplexity)+" useMinimalCoverageHeuristic="+(use_minimal_coverage_heuristic)+" paired="+(paired_reads)+" useIdentityFilter="+(use_identity_filter)+" useSeed=true useCOG=true useKegg=true;\nshow window=seedViewer;\nshow window=seedViewer;\nset context=seedViewer;\nuncollapse nodes=all;\nset context=seedViewer;\nselect nodes=leaves;\nextract what=reads outDir=/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsSEED outFile=reads-%t.fasta data=SEED ids=SELECTED allBelow=true;\nexport what=DSV format=seedpath_readname separator=tab file='/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsSEED/SEEDpath.txt';\ncollapse nodes=top;\nselect nodes=leaves;\nshow chart data=seed;\nset context=SeedChart;\nshow values='true';\nexportImage file="+ (chartdata1) +" format=jpg replace=true;\nshow window=cogViewer;\nshow window=cogViewer;\nset context=cogViewer;\nuncollapse nodes=all;\nset context=cogViewer;\nselect nodes=leaves;\nextract what=reads outDir=/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsCOG outFile=reads-%t.fasta data=COG ids=SELECTED allBelow=true;\nexport what=DSV format=cogpath_readname separator=tab file='/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsCOG/COGpath.txt';\ncollapse nodes=top;\nselect nodes=leaves;\nshow chart data=cog;\nset context=CogChart;\nshow values='true';\nexportImage file="+ (chartdata2) +" format=jpg replace=true;\nshow window=keggViewer;\nshow window=keggViewer;\nset context=keggViewer;\nuncollapse nodes=all;\nset context=keggViewer;\nselect nodes=leaves;\nextract what=reads outDir=/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsKEGG outFile=reads-%t.fasta data=KEGG ids=SELECTED allBelow=true;\nexport what=DSV format=keggpath_readname separator=tab file='/home/anastasia_dev/tmp/"+(unique_id)+"_OutputsKEGG/KEGGpath.txt';\ncollapse nodes=top;\nselect nodes=leaves;\nshow chart data=kegg;\nset context=KeggChart;\nshow values='true';\nexportImage file="+ (chartdata3) +" format=jpg replace=true;\n quit;\n ")
textfile.close() # command file for MEGAN


subprocess.call ('xvfb-run --auto-servernum --server-num=1 MEGAN -g -L /home/ladoukef/programs_various/MEGAN/MEGAN5-academic-license.txt -E < /home/anastasia_dev/tmp/'+unique_id+'_commandsall.txt ' , shell=True) # call MEGAN

os.remove ('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id)) # remove command file

tar = tarfile.open("%s" % (tardata), "w:gz")
for name in ["/home/anastasia_dev/tmp/%s_OutputsSEED" % (unique_id) , "/home/anastasia_dev/tmp/%s_OutputsCOG" % (unique_id) , "/home/anastasia_dev/tmp/%s_OutputsKEGG" % (unique_id)]:
    tar.add(name)
tar.close() # create the tar.gz file


shutil.rmtree("/home/anastasia_dev/tmp/%s_OutputsSEED" % (unique_id)) #remove all tmp output directories
shutil.rmtree("/home/anastasia_dev/tmp/%s_OutputsCOG" % (unique_id))
shutil.rmtree("/home/anastasia_dev/tmp/%s_OutputsKEGG" % (unique_id))

os.remove(b)



 
 

