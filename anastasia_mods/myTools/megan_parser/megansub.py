#!/usr/bin/python
import sys
import subprocess
import string
import os
import csv
import tarfile
import random
import shutil

unique_id= "".join(random.sample(string.letters, 12))

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

meganfile= sys.argv[1] # tool input
rank= sys.argv[2] # taxonomic rank of analysis
chartdata = sys.argv[3] # chart output
txtdata = sys.argv[4] # txt output
tardata = sys.argv[5] # tar.gz output
pick_format = sys.argv[6] # format of the taxonomic tree image
minscore =sys.argv[7] # MEGAN parameter 
maxexpected = sys.argv[8] # MEGAN parameter
toppercent = sys.argv[9] # MEGAN parameter
minsupport = sys.argv[10] # MEGAN parameter
mincomplexity = sys.argv[11] # MEGAN parameter
use_minimal_coverage_heuristic = sys.argv[12] # MEGAN parameter
use_identity_filter = sys.argv[13] # MEGAN parameter
minsupportpercent = sys.argv[14] # MEGAN parameter
lcapercent = sys.argv[15] # MEGAN parameter
paired_reads = sys.argv[16] # MEGAN parameter

a=os.path.realpath(meganfile)
b=a+'.rma'
subprocess.call('cp %s %s' % (a,b) , shell=True)
meganfile=b # add the .rma extension in order to read the input file

flag_all = False
for grp in (sys.argv[1:]):
    if 'all' == grp:
        flag_all = True

if (flag_all): #  all ranks are selected
 
 newpath = ('/home/anastasia_dev/tmp/%s_Outputs' % (unique_id))
 os.makedirs(newpath) #create a directory for output files

 textfile=open('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id),'w')
 textfile.write("open file=" + (meganfile) + ";\nrecompute minSupportPercent="+(minsupportpercent)+" minSupport="+(minsupport)+" minScore="+(minscore)+" maxExpected="+(maxexpected)+" topPercent="+(toppercent)+" lcaPercent="+(lcapercent)+" minComplexity="+(mincomplexity)+" useMinimalCoverageHeuristic="+(use_minimal_coverage_heuristic)+" pairedReads="+(paired_reads)+" useIdentityFilter="+(use_identity_filter)+" ;\n collapse level=100;\n select nodes=all;\n extract what=reads outDir=/home/anastasia_dev/tmp/"+(unique_id)+"_Outputs outFile=reads-%t.fasta data=Taxonomy ids=SELECTED allBelow=false;\n select nodes=none;\n nodeLabels assigned=true;\n show intermediate=true;\n set scaleBy=Assigned;\n zoom full;\n expand direction=vertical;\n expand direction=vertical;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n exportImage file=/home/anastasia_dev/tmp/" +(unique_id)+ "_tree format=" +(pick_format)+ " replace=true;\n select nodes=all;\n show chart data=taxonomy;\n set context=TaxaChart;\n  show values=true;\n export what=chartData file=" +(txtdata)+ ";\n exportImage file= "+(chartdata)+ " format=jpg replace=true;\n quit;\n " )
 textfile.close() # create a command file for MEGAN


 subprocess.call ('xvfb-run --auto-servernum --server-num=1 MEGAN -g -L /home/ladoukef/programs_various/MEGAN/MEGAN5-academic-license.txt -E < /home/anastasia_dev/tmp/'+unique_id+'_commandsall.txt ' , shell=True) # call MEGAN

 os.remove ('/home/anastasia_dev/tmp/%s_commandsall.txt' % (unique_id)) #remove command file
 
 tar = tarfile.open("%s" % (tardata), "w:gz")
 for name in ["/home/anastasia_dev/tmp/%s_tree" % (unique_id) , "/home/anastasia_dev/tmp/%s_Outputs" % (unique_id)]:
    tar.add(name)
 tar.close() # create the tar.gz file 
 
 os.remove ("/home/anastasia_dev/tmp/%s_tree" % (unique_id)) #remove tmp tree file
 #os.remove ("%s_chartdata.txt" % (unique_id))
 #os.remove ( "%s_chart" % (unique_id))
 shutil.rmtree("/home/anastasia_dev/tmp/%s_Outputs" % (unique_id)) #remove tmp Outputs directory 

else: # a speficic rank is selected
 
 newpath = ('/home/anastasia_dev/tmp/%s_Outputs' % (unique_id) )
 os.makedirs(newpath) #create a directory for output files

 textfile=open('/home/anastasia_dev/tmp/%s_commandsrank.txt' % (unique_id),'w')
 textfile.write("open file=" + (meganfile) + ";\nrecompute minSupportPercent="+(minsupportpercent)+" minSupport="+(minsupport)+" minScore="+(minscore)+" maxExpected="+(maxexpected)+" topPercent="+(toppercent)+" lcaPercent="+(lcapercent)+" minComplexity="+(mincomplexity)+" useMinimalCoverageHeuristic="+(use_minimal_coverage_heuristic)+" pairedReads="+(paired_reads)+" useIdentityFilter="+(use_identity_filter)+" ;\n\n update;\n collapse rank=" + (rank) + ";\n nodeLabels assigned=true;\n show intermediate=true;\n set scaleBy=Assigned;\n zoom full;\n expand direction=vertical;\n expand direction=vertical;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n expand direction=horizontal;\n exportImage file=/home/anastasia_dev/tmp/"+ (unique_id) +"_tree format="+(pick_format)+" replace=true;\n select rank=" + (rank) + ";\n extract what=reads outDir=/home/anastasia_dev/tmp/"+(unique_id)+"_Outputs outFile=reads-%t.fasta data=Taxonomy ids=SELECTED allBelow=true;\n show chart data=taxonomy;\n set context=TaxaChart;\n show values=true;\n export what=chartData file=/home/anastasia_dev/tmp/"+ (unique_id) +".txt;\n exportImage file="+(chartdata)+" format=jpg replace=true;\n  quit;\n " )
 textfile.close() # create a command file for MEGAN

 subprocess.call ('xvfb-run --auto-servernum --server-num=1 MEGAN -g -L /home/ladoukef/programs_various/MEGAN/MEGAN5-academic-license.txt -E < /home/anastasia_dev/tmp/'+unique_id+'_commandsrank.txt' , shell=True) # call MEGAN 

 os.remove ('/home/anastasia_dev/tmp/%s_commandsrank.txt' % (unique_id)) #remove command file

 found=-1
 names = []
 values = []
 
 with open('/home/anastasia_dev/tmp/%s.txt' % (unique_id), 'r') as csv:
    
    for line in csv.readlines():
        if found==-1:
          elements = line.strip().split('\t')
          found=1
        else:
          elements = line.strip().split('\t')
          values.append(float(elements[1]))
          names.append(elements[0])
 csum = sum(values)

 percentage = []
 for x in values:
   percentage.append((x/csum)*100) # calculate percentages


 file=open('%s' % (txtdata),'w')
 file.write(" #Series " +"\t"+ "Reads" + "\t" + "Percentages" +"\n")
 for i,j,k in zip (names,values,percentage):
  file.write(str(i) + "\t" +str(j)+ "\t" +str(k) +"\n")
 file.close() # create a new txt file containing percentages
 
 os.remove('/home/anastasia_dev/tmp/%s.txt' % (unique_id)) # remove tmp txt file

 tar = tarfile.open("%s" % (tardata), "w:gz")
 for name in ["/home/anastasia_dev/tmp/%s_tree" % (unique_id) , "/home/anastasia_dev/tmp/%s_Outputs" % (unique_id)]:
    tar.add(name)
 tar.close() #create the tar.gz 

 os.remove ("/home/anastasia_dev/tmp/%s_tree" % (unique_id)) # remove tmp tree file 
 #os.remove ('/home/anastasia_dev/tmp/%s_numreads' % (unique_id))
 #os.remove ( '%s_percentage' % (unique_id))
 shutil.rmtree("/home/anastasia_dev/tmp/%s_Outputs" % (unique_id)) # remove tmp Outputs directory

 
os.remove(b)









