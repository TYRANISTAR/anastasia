#!/usr/bin/python
#
#usage: python contig_db.py contig_file dump.sql database_datafile

import sys, getopt, parser, csv, MySQLdb, random, string, subprocess, datetime
from Bio import SeqIO

assert sys.version_info[:2] >= ( 2, 4 )

#create a unique identifier (random variable for id) for each user
letandnum='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
unique_id= "".join(random.sample(letandnum, 20))
	
#check the date
day=str(datetime.date.today())
day=day.replace('-','_')
unique_id=unique_id+'_'+day

def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()
	
def __main__():	
	#MySQL connection
	#dbs_db=sys.argv[3]
	#dbs_user=sys.argv[4]
	#dbs_passwd=sys.argv[5]	

	database_datafile = open(sys.argv[3], 'r')
	for i,line in enumerate(database_datafile):
		if i==0:
			dbs_db=line.replace('\n','')
		elif i==1:
			dbs_user=line.replace('\n','')
		elif i==2:
			dbs_passwd=line.replace('\n','')
		elif i>2:
			break
	database_datafile.close()

	db = MySQLdb.connect(host="127.0.0.1", user=dbs_user, port=3306, passwd=dbs_passwd, db=dbs_db)
	
	#cursor object that allows MySQL queries
	cur = db.cursor()
		
	#create table for genes command
	seq_tbl="create table %s_seq_tbl (gene_id varchar(100), gene_name varchar(100), sequence MEDIUMTEXT, seq_length integer(5), description varchar(100))" % (unique_id,)
	cur.execute(seq_tbl)
		
	#insert sequence data to MySQL database
	for seq_record in SeqIO.parse(sys.argv[1], 'fasta'):
		seq_record.description=seq_record.description.replace(" ","_")
		values=(seq_record.id, seq_record.name, seq_record.seq._data, len(seq_record), seq_record.description)
		cur.execute('insert into %s_seq_tbl (gene_id, gene_name, sequence, seq_length, description) values %s' % (unique_id, str(values))) 
		db.commit()			
	
	#mysqldump
	subprocess.call('mysqldump -u %s -p%s %s %s_seq_tbl > %s' % (dbs_user, dbs_passwd, dbs_db, unique_id, sys.argv[2]), shell=True)
	
	#drop mysql tables for house cleaning
	drop_tbl="drop table %s_seq_tbl" % (unique_id,)
	cur.execute(drop_tbl)

	
if __name__ == "__main__": __main__()
