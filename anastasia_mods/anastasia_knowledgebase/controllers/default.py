# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
input_form=FORM('', INPUT(_name='name',
    requires=IS_NOT_EMPTY()),
    INPUT(_type='submit'))

@auth.requires_membership('manager')
def index():
    if input_form.accepts(request,session):
        table_name=request.vars.name
        try:
            akn_db[table_name]
            redirect(URL('manage_makis', vars=dict(table_name=table_name)))
        except AttributeError:
            response.flash = 'Incorrect Job ID. Please check your ANASTASIA instance'
    else:
        response.flash = 'Please enter a Job ID from ANASTASIA'
    return dict(form=input_form)

@auth.requires_membership('manager')
def manage_makis():
    response.flash = 'This is the dataset you selected'
    table=request.vars.table_name
    form3=SQLFORM.grid(akn_db[table], create=False, deletable=False, editable=False, maxtextlength=32, paginate=10)
    return dict(form3=form3)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
