#!/usr/bin/python
#
# usage: eficaz.py orfile.fasta resultfile.fasta.ecpred
#
#

import sys, fileinput, string, subprocess, os
from Bio import SeqIO


assert sys.version_info[:2] >= ( 2, 4 )



def stop_err( msg ):
    sys.stderr.write( "%s\n" % msg )
    sys.exit()
	
def __main__():	
	#assigning files
	input_file=sys.argv[1]
	output_file=sys.argv[2]
	filename=os.path.basename(input_file)
	filepath=os.path.abspath(input_file)
	
	#assigning root directory
	root_dir='/hotzyme/makis/other_programs/EFICAz2.5.1/bin'
	os.chdir(root_dir)
	
	#copying files to EFICAz directory
	
	subprocess.call('cp %s %s' % (filepath, root_dir), shell=True)
	
	#running EFICAz
	subprocess.call('./eficaz2.5 %s' % (filename), shell=True)
	
	#renaming output file
	original_name="".join(['%s' % (filename), '.ecpred'])
	subprocess.call('mv %s/%s %s' % (root_dir, original_name, output_file), shell=True)
	
	#deleting file from EFICAz directory
	subprocess.call('rm %s' % (filename), shell=True)
	
			
if __name__ == "__main__": __main__()



