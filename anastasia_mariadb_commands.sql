create schema anastasia_knowledgebase;
create user 'anastasia'@'localhost' identified by 'anastas1a';
GRANT ALL PRIVILEGES ON anastasia_knowledgebase.* TO 'anastasia'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON `knowledgebase\_%` . * TO 'anastasia'@'%';	
GRANT FILE ON *.* TO 'anastasia'@'localhost';