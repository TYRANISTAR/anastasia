#!/usr/bin/bash
cur_dir=$(pwd)
cur_user=$USER
extra_dir=$(pwd)/ANASTASIA/anastasia_mods

echo 'Installing software for ANASTASIA tools' #to do: make automated script to find latest release for each tool
mkdir $cur_dir/anastasia_software
sudo chown -R $cur_user:anastasia_group $cur_dir/anastasia_software
sudo chmod -R 775 $cur_dir/anastasia_software

#hmmer
cd $cur_dir/anastasia_software
wget http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2.tar.gz
tar -zxvf hmmer-3.1b2.tar.gz
cd hmmer-3.1b2
./configure
make
sudo make install

#fastx
cd $cur_dir/anastasia_software
wget https://github.com/agordon/fastx_toolkit/releases/download/0.0.14/fastx_toolkit-0.0.14.tar.bz2
tar -xjf fastx_toolkit-0.0.14.tar.bz2
cd fastx_toolkit-0.0.14
./configure
make
sudo make install

#cd-hit
cd $cur_dir/anastasia_software
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/cdhit/cd-hit-v4.6.1-2012-08-27.tgz
tar xvf cd-hit-v4.6.1-2012-08-27.tgz
cd cd-hit-v4.6.1-2012-08-27
make
sudo make install

#emboss
cd $cur_dir/anastasia_software
wget ftp://emboss.open-bio.org/pub/EMBOSS/emboss-latest.tar.gz
tar -zxvf emboss-latest.tar.gz
cd EMBOSS-6.6.0
./configure
make
sudo make install

#fastqc
cd $cur_dir/anastasia_software
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5_source.zip
unzip fastqc_v0.11.5_source.zip
cd FastQC
chmod 755 fastqc
sudo ln -s $(pwd)/fastqc /usr/local/bin/fastqc

#samtools
cd $cur_dir/anastasia_software
wget https://downloads.sourceforge.net/project/samtools/samtools/1.3.1/samtools-1.3.1.tar.bz2
tar -xjf samtools-1.3.1.tar.bz2
cd samtools-1.3.1
./configure
make
sudo make install

#bowtie2
cd $cur_dir/anastasia_software
wget https://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.3.0/bowtie2-2.3.0-source.zip
unzip bowtie2-2.3.0-source.zip
cd bowtie2-2.3.0
make
sudo make install

#velvet
cd $cur_dir/anastasia_software
wget https://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.10.tgz
tar -xvf velvet_1.2.10.tgz
cd velvet_1.2.10
make MAXKMERLENGTH=151 OPENMP=1
sudo ln -s $(pwd)/velveth /usr/local/bin/velveth
sudo ln -s $(pwd)/velvetg /usr/local/bin/velvetg

#megahit
cd $cur_dir/anastasia_software
git clone https://github.com/voutcn/megahit.git
cd megahit 
make
sudo ln -s $(pwd)/megahit /usr/local/bin/megahit
sudo ln -s $(pwd)/megahit_asm_core /usr/local/bin/megahit_asm_core
sudo ln -s $(pwd)/megahit_sdbg_build /usr/local/bin/megahit_sdbg_build
sudo ln -s $(pwd)/megahit_toolkit /usr/local/bin/megahit_toolkit

#prodigal
cd $cur_dir/anastasia_software
wget https://github.com/hyattpd/Prodigal/archive/v2.6.3.zip
unzip v2.6.3.zip
cd Prodigal-2.6.3/
make
make install

#Minpath
cd $cur_dir/anastasia_software
mv $extra_dir/minpath1.2.tar.gz ./
tar -zxvf minpath1.2.tar.gz
cd MinPath
sudo ln -s $(pwd)/MinPath1.2.py /usr/local/bin/MinPath1.2.py

#MEGAN
cd $cur_dir/anastasia_software
wget http://ab.inf.uni-tuebingen.de/data/software/megan5/download/MEGAN_unix_5_11_3.sh
sudo sh MEGAN_unix_5_11_3.sh
cp $extra_dir/MEGAN5-academic-license.txt ./

#Usearch
sudo cp $extra_dir/usearch9.2.64_i86linux32 /usr/local/bin/usearch

#blast
cd $cur_dir/anastasia_software
wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.6.0+-src.tar.gz
tar -zxvf ncbi-blast-2.6.0+-src.tar.gz
cd ncbi-blast-2.6.0+-src/c++/
./configure
make
sudo make install

#Eficaz2
cd $cur_dir/anastasia_software
wget http://cssb2.biology.gatech.edu/4176ef47-d63a-4dd8-81df-98226e28579e/EFICAz2.5.1.tar.gz
tar -zxvf EFICAz2.5.1.tar.gz
cd ./Eficaz2.5.1/bin
sudo ./INSTALL

#-----------------------------
