#!/bin/bash

cur_dir=$(pwd)
cur_user=$USER
only_dir=${PWD##*/}
sudo chmod 755 $(pwd)
cur_ip=$(hostname -I | cut -d" " -f1)
error_message=""
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    if cat /etc/*-release | grep "debian"; then #installation for Debian
		echo "You seem to have Debian OS"
		
		#Galaxy/ANASTASIA dependencies
		echo 'Installing dependencies for Galaxy...'
		sudo apt-get -y update
		sudo apt-get -y install zip
		sudo apt-get -y install npm 
		sudo apt-get -y install gcc
		sudo apt-get -y install g++
		sudo npm install -g grunt grunt-cli
		sudo apt-get -y install git
		sudo apt-get -y install mercurial
		sudo apt-get -y install python
		sudo apt-get -y install python-pip
		sudo pip install --upgrade pip
		sudo apt-get -y install build-essential		
		sudo pip install uwsgi 
		sudo pip install sklearn
		sudo pip install pymongo==3.1.1
		sudo pip install pandas
		sudo pip install scipy
		sudo apt-get -y install perl
		sudo apt-get -y install perl-Module-Build
		sudo apt-get -y install perl-IO-String
		sudo apt-get -y install perl-CPAN 
		sudo apt-get -y install curl
		sudo apt-get -y install wget
		sudo apt-get -y install libc6
		sudo apt-get -y install openssl
		sudo apt-get -y install libffi6
		sudo apt-get -y install openssl
		sudo apt-get -y install libxml2
		sudo apt-get -y install libxslt1.1
		sudo apt-get -y install java
		sudo apt-get -y install tar
		sudo apt-get -y install libgomp1
		sudo apt-get -y install lynx
		sudo apt-get -y install glpk-utils
		sudo apt-get -y install apache2
		sudo service apache2 start
		sudo apt-get -y install php 
		sudo apt-get -y install php-mysql
		sudo apt-get -y install libapache2-mod-wsgi
		echo 'Installation of dependencies of Galaxy complete'
		
		#ANASTASIA tools' dependencies
		echo 'Installing dependencies for ANASTASIA tools...'
		sudo apt-get -y install libstdc++6 #for megahit
		sudo apt-get -y install libncurses5 #for samtools
		sudo apt-get -y install xorg openbox #for megan
		sudo apt-get -y install libtbb-dev #for bowtie2
		sudo apt-get -y install libcurl4-gnutls-dev #for samtools  
		sudo apt-get -y install libcurl4-openssl-dev #for samtools  
		sudo apt-get -y install mongodb #for hydrolase classifier
		sudo apt-get -y install mongodb-server #for hydrolase classifier
		sudo service mongod start #for hydrolase classifier
		echo 'Installation of dependencies for ANASTASIA tools completed'
		
		echo 'Installing MariaDB...'
		if command -v mysql; then
		#in case it's stopped
			sudo apt-get -y install libmysqlclient-dev
			sudo /etc/init.d/mysql start	
			#sudo mysql -e "create schema anastasia_galaxy"
			sudo mysql -e "create schema anastasia_knowledgebase"
			sudo mysql -e "create user 'anastasia'@'localhost' identified by 'anastas1a'"
			#sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_galaxy.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_knowledgebase.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON \`knowledgebase\_%\` . * TO 'anastasia'@'%'"
			sudo mysql -e "GRANT FILE ON *.* TO 'anastasia'@'localhost'"	
			sudo pip install MySQL-python				
			echo "-----------------------------------------------------------------------------------------------------------"
			echo "You already have a MariaDB or a MySQL installation. Please log in as root and paste commands found in ANASTASIA/anastasia_mariadb_commands.txt:"
			error_message="You already have a MariaDB or a MySQL installation. Please log in as root and paste commands found in ANASTASIA/anastasia_mariadb_commands.sql:"
			echo "-----------------------------------------------------------------------------------------------------------"
		else
			sudo apt-get -y install mariadb-server #for ANASTASIA knowledgebase
			sudo apt-get -y install libmysqlclient-dev
			sudo /etc/init.d/mysql start
			#sudo mysql -e "create schema anastasia_galaxy"
			sudo mysql -e "create schema anastasia_knowledgebase"
			sudo mysql -e "create user 'anastasia'@'localhost' identified by 'anastas1a'"
			#sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_galaxy.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_knowledgebase.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON \`knowledgebase\_%\` . * TO 'anastasia'@'%'"
			sudo mysql -e "GRANT FILE ON *.* TO 'anastasia'@'localhost'"	
			sudo pip install MySQL-python			
			echo 'Installation of MariaDB complete'
		fi		
			
		echo "Configuring Apache web server..."
		sudo touch /etc/apache2/sites-available/000-default.conf_before_anastasia
		sudo chmod 777 /etc/apache2/sites-available/000-default.conf_before_anastasia
		sudo cat /etc/apache2/sites-available/000-default.conf >> /etc/apache2/sites-available/000-default.conf_before_anastasia
		sed -i "s/=apache/=www-data/g" $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf #because debian and apache need special care
		sed -i "s/anastasia_web/$only_dir/g" $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf
		sed -i "s%/home/$only_dir%$cur_dir%g" $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf
		sudo cp $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf /etc/apache2/sites-available/000-default.conf
		sudo ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load 
		#source /etc/apache2/envvars
		sudo service apache2 restart		
		echo "Apache web server configured please check /etc/apache2/sites-available/ for the new configuration files"
		
		echo 'Installing web2py and ANASTASIA knowledgebase...'		
		cd $cur_dir
		wget https://mdipierro.pythonanywhere.com/examples/static/web2py_src.zip
		unzip -q web2py_src.zip	
		cp -r $cur_dir/ANASTASIA/anastasia_mods/anastasia_knowledgebase $cur_dir/web2py/applications/	
		sudo groupadd anastasia_group
		sudo usermod -a -G anastasia_group $cur_user
		sudo chmod -R 775 $cur_dir/web2py
		sudo chown -R www-data:anastasia_group $cur_dir/web2py
		nohup python $cur_dir/web2py/web2py.py -a 'anastas1a' -i 0.0.0.0 -p 8003 & #in case you're already using port 8000
		sudo service apache2 restart		
		echo 'web2py and ANASTASIA knowledgebase installation complete. The username/password for the ANASTASIA knowledgebase are anastasia@user.com/anastas1a'
		echo 'The password for web2py administrative interface is anastas1a'
		
		echo "Starting ANASTASIA..."
		cd $cur_dir
		sudo sh $cur_dir/galaxy/run.sh --stop-daemon #if it exists
		sudo rm -rf $cur_dir/galaxy/ #if it exists
		git clone -b release_16.07 https://github.com/galaxyproject/galaxy.git 
		cp $cur_dir/ANASTASIA/anastasia_mods/config/galaxy.ini $cur_dir/galaxy/config/
		cp $cur_dir/ANASTASIA/anastasia_mods/config/*.xml $cur_dir/galaxy/config/
		cp $cur_dir/ANASTASIA/anastasia_mods/static/welcome.html $cur_dir/galaxy/static/
		sed -i "s%motherbox.chemeng.ntua.gr/%$cur_ip:8003/%g" $cur_dir/galaxy/static/welcome.html	#8003 is the port we are assigning to web2py	
		cp -r $cur_dir/ANASTASIA/anastasia_mods/static/feimg/ $cur_dir/galaxy/static/
		cp $cur_dir/ANASTASIA/tool-data/*.loc $cur_dir/galaxy/tool-data/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/ $cur_dir/galaxy/tools/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/fastx_toolkit/ $cur_dir/galaxy/tools/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/ncbi_blast_plus/ $cur_dir/galaxy/tools/
		sed -i '/        analysisPage.right.historyView.connectToQuotaMeter( analysisPage.masthead.quotaMeter );/a\        + analysisPage.right.toggle();\n        + analysisPage.left.toggle();' $(pwd)/galaxy/client/galaxy/scripts/apps/analysis.js
		cd $cur_dir/galaxy/client/
		sudo npm install -g grunt grunt-cli
		npm install --no-optional #debian issue
		sudo ln -s /usr/bin/nodejs /usr/bin/node #debian issue
		grunt	
		cd $cur_dir/galaxy/		
		sudo chown -R $cur_user:anastasia_group $cur_dir/galaxy	
		sudo chmod -R 775 $cur_dir/galaxy	
		
		#repairing some paths in ANASTASIA tools
		mkdir $cur_dir/tmp/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/ec_kegg/ $cur_dir/tmp/		
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%ladoukef%anastasia_temp%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%ladoukef%anastasia_temp%g" {} \;		
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%anastasia_dev%anastasia_temp%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%anastasia_dev%anastasia_temp%g" {} \;		
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/ncbi_databases/%$cur_dir/anastasia_software/ncbi_databases/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/ncbi_databases/%$cur_dir/anastasia_software/ncbi_databases/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/Pfam_database/%$cur_dir/anastasia_software/Pfam_database/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/Pfam_database/%$cur_dir/anastasia_software/Pfam_database/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/programs_various/%$cur_dir/anastasia_software/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/programs_various/%$cur_dir/anastasia_software/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/tmp/%$cur_dir/tmp/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%motherbox.chemeng.ntua.gr/anastasia_knowledgebase%$cur_ip:8003/anastasia_knowledgebase%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/web2py_usr/web2py/%$cur_dir/web2py/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/galaxy/tools/%$cur_dir/galaxy/tools/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/%$cur_dir/%g" {} \;
		sudo chown mysql:anastasia_group $cur_dir/tmp/
		sudo chmod 775 $cur_dir/tmp/ 	
		
		sh run.sh --daemon && cd $cur_dir/galaxy/.venv
		source bin/activate
		pip install biopython
		pip install sklearn
		pip install pymongo==3.1.1
		pip install pandas
		pip install scipy	
		pip install MySQL-python
		deactivate		
		echo "-----------------------------------------------------------------------------------------------------------"	
		echo "ANASTASIA is running but it's not fully operational. Please run sh anastasia_tool_install.sh for installing the necessary software used by the platform"
		echo "Check ANASTASIA in 0.0.0.0:8092/anastasia_dev/ or 127.0.0.1:8092/anastasia_dev/ in a local computer"
		echo "-----------------------------------------------------------------------------------------------------------"	
		
	elif cat /etc/*-release | grep "centos"; then #installation for CentOS
		echo "You seem to have CentOS distribution"	
		echo "Disabling SELinux..."
		sudo setenforce 0
		echo "SELinux disabled"
		#Galaxy/ANASTASIA dependencies
		echo 'Installing dependencies for Galaxy...'
		sudo yum -y update
		sudo yum -y --enablerepo=extras install epel-release
		sudo yum -y install zip
		sudo yum -y install npm 
		sudo yum -y install gcc
		sudo yum -y install gcc-c++
		sudo npm install -g grunt grunt-cli
		sudo yum -y install git
		sudo yum -y install mercurial
		sudo yum -y install python python-devel
		sudo yum -y install python-pip
		sudo pip install --upgrade pip
		sudo yum -y groupinstall 'Development Tools'
		sudo pip install uwsgi 
		sudo pip install sklearn
		sudo pip install pymongo==3.1.1
		sudo pip install pandas
		sudo pip install scipy		
		sudo yum -y install perl
		sudo yum -y install perl-Module-Build
		sudo yum -y install perl-IO-String
		sudo yum -y install perl-CPAN 
		sudo yum -y install curl
		sudo yum -y install wget
		sudo yum -y install glibc.i686
		sudo yum -y install openssl
		sudo yum -y install libffi 
		sudo yum -y install openssl
		sudo yum -y install libxml2
		sudo yum -y install libxslt
		sudo yum -y install java
		sudo yum -y install tar
		sudo yum -y install libgomp
		sudo yum -y install lynx
		sudo yum -y install glpk
		sudo yum -y install httpd
		sudo service httpd start
		sudo chkconfig httpd on
		sudo chkconfig mysqld on
		sudo yum -y install php php-mysql
		sudo yum -y install mod_wsgi
		
		echo 'Installation of dependencies of Galaxy complete'
		
		#ANASTASIA tools' dependencies
		echo 'Installing dependencies for ANASTASIA tools...'
		sudo yum -y install libstdc++-static #for megahit
		sudo yum -y install ncurses-devel #for samtools
		sudo yum -y install xorg-x11-server-Xvfb #for megan
		sudo yum -y install tbb-devel #for bowtie2
		sudo yum -y install libcurl #for samtools    
		sudo yum -y install mongodb #for hydrolase classifier
		sudo yum -y install mongodb-server #for hydrolase classifier
		sudo service mongod start
		echo 'Installation of dependencies for Galaxy complete'
		
		echo 'Installing MariaDB...'
		if command -v mysql; then
			sudo systemctl start mariadb
			#in case it's stopped
			#sudo mysql -e "create schema anastasia_galaxy"
			sudo mysql -e "create schema anastasia_knowledgebase"
			sudo mysql -e "create user 'anastasia'@'localhost' identified by 'anastas1a'"
			#sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_galaxy.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_knowledgebase.* TO 'anastasia'@'localhost' WITH GRANT OPTION"	
			sudo mysql -e "GRANT ALL PRIVILEGES ON \`knowledgebase\_%\` . * TO 'anastasia'@'%'"	
			sudo mysql -e "GRANT FILE ON *.* TO 'anastasia'@'localhost'"	
			echo "-----------------------------------------------------------------------------------------------------------"
			echo "You already have a MariaDB or a MySQL installation. Please log in as root and paste commands found in ANASTASIA/anastasia_mariadb_commands.txt:"
			error_message="You already have a MariaDB or a MySQL installation. Please log in as root and paste commands found in ANASTASIA/anastasia_mariadb_commands.sql:"
			echo "-----------------------------------------------------------------------------------------------------------"
			sudo yum -y install mariadb-libs mariadb-devel	
			sudo pip install MySQL-python				
		else
			sudo yum -y install mariadb-server #for ANASTASIA knowledgebase
			sudo yum -y install mariadb-libs mariadb-devel
			sudo systemctl start mariadb
			#sudo mysql -e "create schema anastasia_galaxy"
			sudo mysql -e "create schema anastasia_knowledgebase"
			sudo mysql -e "create user 'anastasia'@'localhost' identified by 'anastas1a'"
			#sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_galaxy.* TO 'anastasia'@'localhost' WITH GRANT OPTION"
			sudo mysql -e "GRANT ALL PRIVILEGES ON anastasia_knowledgebase.* TO 'anastasia'@'localhost' WITH GRANT OPTION"	
			sudo mysql -e "GRANT ALL PRIVILEGES ON \`knowledgebase\_%\` . * TO 'anastasia'@'%'"	
			sudo mysql -e "GRANT FILE ON *.* TO 'anastasia'@'localhost'"	
			sudo pip install MySQL-python			
			echo 'Installation of MariaDB complete'
		fi	
		
		echo "Configuring Apache web server..."
		sudo touch /etc/httpd/conf/httpd_before_anastasia
		sudo chmod 777 /etc/httpd/conf/httpd_before_anastasia
		sudo cat /etc/httpd/conf/httpd.conf >> /etc/httpd/conf/httpd_before_anastasia
		sed -i "s/anastasia_web/$only_dir/g" $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf
		sed -i "s%/home/$only_dir%$cur_dir%g" $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf
		sudo cat /etc/httpd/conf/httpd.conf $cur_dir/ANASTASIA/anastasia_mods/apache_mod_anastasia.conf > /etc/httpd/conf/httpd_temp.conf
		sudo cp /etc/httpd/conf/httpd_temp.conf /etc/httpd/conf/httpd.conf
		sudo apachectl restart
		echo "Apache web server configured please check /etc/httpd/conf/ for the new configuration files"
		
		echo 'Installing web2py...'		
		cd $cur_dir
		wget https://mdipierro.pythonanywhere.com/examples/static/web2py_src.zip
		unzip -q web2py_src.zip	
		sudo groupadd anastasia_group
		sudo usermod -a -G anastasia_group $cur_user
		sudo chmod -R 775 $cur_dir/web2py
		sudo chown -R apache:anastasia_group $cur_dir/web2py		
		cp -r $cur_dir/ANASTASIA/anastasia_mods/anastasia_knowledgebase $cur_dir/web2py/applications/
		nohup python $cur_dir/web2py/web2py.py -a 'anastas1a' -i 0.0.0.0 -p 8003 & #in case you're already using port 8000
		sudo service h restart	
		echo 'web2py and ANASTASIA knowledgebase installation complete. The username/password for the ANASTASIA knowledgebase are anastasia@user.com/anastas1a'
		echo 'The password for web2py administrative interface is anastas1a'
		
		echo "Starting ANASTASIA..."
		cd $cur_dir
		sudo sh $cur_dir/galaxy/run.sh --stop-daemon #if it exists
		sudo rm -rf $cur_dir/galaxy/ #if it exists
		git clone -b release_16.07 https://github.com/galaxyproject/galaxy.git 
		cp $cur_dir/ANASTASIA/anastasia_mods/config/galaxy.ini $cur_dir/galaxy/config/
		cp $cur_dir/ANASTASIA/anastasia_mods/config/*.xml $cur_dir/galaxy/config/
		cp $cur_dir/ANASTASIA/anastasia_mods/static/welcome.html $cur_dir/galaxy/static/
		sed -i "s%motherbox.chemeng.ntua.gr%$cur_ip:8003%g" $cur_dir/galaxy/static/welcome.html	#8003 is the port we are assigning to web2py	
		cp -r $cur_dir/ANASTASIA/anastasia_mods/static/feimg/ $cur_dir/galaxy/static/
		cp $cur_dir/ANASTASIA/tool-data/*.loc $cur_dir/galaxy/tool-data/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/ $cur_dir/galaxy/tools/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/fastx_toolkit/ $cur_dir/galaxy/tools/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/myTools/ncbi_blast_plus/ $cur_dir/galaxy/tools/
		sed -i '/        analysisPage.right.historyView.connectToQuotaMeter( analysisPage.masthead.quotaMeter );/a\        + analysisPage.right.toggle();\n        + analysisPage.left.toggle();' $(pwd)/galaxy/client/galaxy/scripts/apps/analysis.js
		cd $cur_dir/galaxy/client/
		sudo npm install -g grunt grunt-cli
		sudo npm install
		sudo grunt
		cd $cur_dir/galaxy/
		sudo chown -R $cur_user:anastasia_group $cur_dir/galaxy	
		sudo chmod -R 775 $cur_dir/galaxy	
		
		#repairing some paths in ANASTASIA tools
		mkdir $cur_dir/tmp/
		cp -r $cur_dir/ANASTASIA/anastasia_mods/ec_kegg/ $cur_dir/tmp/		
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%ladoukef%anastasia_temp%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%ladoukef%anastasia_temp%g" {} \;		
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%anastasia_dev%anastasia_temp%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%anastasia_dev%anastasia_temp%g" {} \;		
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/ncbi_databases/%$cur_dir/anastasia_software/ncbi_databases/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/ncbi_databases/%$cur_dir/anastasia_software/ncbi_databases/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/Pfam_database/%$cur_dir/anastasia_software/Pfam_database/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/Pfam_database/%$cur_dir/anastasia_software/Pfam_database/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/programs_various/%$cur_dir/anastasia_software/%g" {} \;
		sudo find $cur_dir/galaxy/tool-data/ -type f -exec sed -i -e "s%/home/anastasia_temp/programs_various/%$cur_dir/anastasia_software/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/tmp/%$cur_dir/tmp/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%motherbox.chemeng.ntua.gr/anastasia_knowledgebase%$cur_ip:8003/anastasia_knowledgebase%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/web2py_usr/web2py/%$cur_dir/web2py/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/galaxy/tools/%$cur_dir/galaxy/tools/%g" {} \;
		sudo find $cur_dir/galaxy/tools/ -type f -exec sed -i -e "s%/home/anastasia_temp/%$cur_dir/%g" {} \;
		sudo chown mysql:anastasia_group $cur_dir/tmp/
		sudo chmod 775 $cur_dir/tmp/ 
		
		sh run.sh --daemon && cd $cur_dir/galaxy/.venv
		source bin/activate
		pip install biopython
		pip install sklearn
		pip install pymongo==3.1.1
		pip install pandas
		pip install scipy	
		pip install MySQL-python
		deactivate		
		echo "-----------------------------------------------------------------------------------------------------------"		
		echo "ANASTASIA is running but it's not fully operational. Please run sh anastasia_tool_install.sh for installing the necessary software used by the platform"
		echo "Check ANASTASIA in 0.0.0.0:8092/anastasia_dev/ or 127.0.0.1:8092/anastasia_dev/ in a local computer"		
		echo "Keep in mind that SELinux is now disabled. In order for you to enable it again you can type:"
		echo "sudo setenforce 1"
		echo "-----------------------------------------------------------------------------------------------------------"	
	
	fi


elif [[ "$OSTYPE" == "dwin"* ]]; then
	echo "ANASTASIA currently doesn't support MacOS distributions. Exiting now."
else
    echo "Unknown OS exiting now."
fi


echo $error_message
